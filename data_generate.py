import random

import str_random

# 参数
# 最大指令条数
MAX_INSTR_NUM = 1000
# 最大瓶子容量
MAX_BOTTLE_CAPACITY = 100
# 最小瓶子容量
MIN_BOTTLE_CAPACITY = 10
# 初始最大星级
MAX_INIT_STAR = 5
# 食物最大能量
MAX_FOOD_ENERGY = 5
# 最多冒险者数目
MAX_ADV_NUM = 5
# 不同name的列表大小，控制大小防止没有重复的
not_adv_name_num = 1
# 第几次作业
course_number = 2  # 1,2

# 变量
# id 池，保证出现过的 id 不一样
id_pool = set()
#  冒险者 id 表
adventure_id_list = []
# 冒险者的 bottles 的 id
adventure_bottle_dict = {}
# ----
adventure_equipment_dict = {}
# ----
adventure_food_dict = {}
# id 对应 name 的字典
id_2_name_dict = {}
# 已经空的瓶子的id集合，每次使用瓶子的时候查询，假设该瓶子空了，则从adventure的dict里移除
empty_bottle_pool = set()
# 人名集合，保证人名不重复
adventure_name_pool = set()
# 非人名列表,为了制造重复，限定仅有很小的一部分name存在
not_adv_name_list = []


# 由于保证药水瓶无替换功能，所以空的药水瓶必然已被携带


# 根据课程第几次作业动态调节指令的最大序号
def generate_instr_sequence():
    if course_number == 1:
        return 6
    elif course_number == 2:
        return 13


# 不用管
def init_new_adventure_id(new_id):
    adventure_id_list.append(new_id)
    adventure_bottle_dict[new_id] = []
    adventure_equipment_dict[new_id] = []
    adventure_food_dict[new_id] = []


# 初始要设置的冒险者数目
def init_env(num):
    for i in range(0, not_adv_name_num):
        not_adv_name_list.append(str_random.getStr())
    for i in range(0, num):
        instr = ""
        instr += "1 "
        new_id = get_new_id()
        instr += str(new_id) + " "
        init_new_adventure_id(new_id)
        instr += str_random.getStr()
        print(instr)


# 生成指令， num 是生成多少条
def generate_instr(num):
    count = 0
    adv_count = 0
    while count < num:
        instr_number = random.randint(1, generate_instr_sequence())
        instr = ""
        count += 1
        if instr_number == 1:
            instr += "1 "
            if adv_count == MAX_ADV_NUM:
                count -= 1
                continue
            new_id = get_new_id()
            instr += str(new_id) + " "
            init_new_adventure_id(new_id)
            instr += get_adv_new_name()
            print(instr)
            adv_count += 1
        elif instr_number == 2:
            instr += "2 "
            adv_id = get_exist_adventure_id()
            bot_id = get_new_id()
            bot_name = get_not_adv_name()
            id_2_name_dict[bot_id] = bot_name
            instr += str(adv_id) + " "
            instr += str(bot_id) + " "
            instr += str(bot_name) + " "
            instr += str(random.randint(MIN_BOTTLE_CAPACITY, MAX_BOTTLE_CAPACITY))
            adventure_bottle_dict.get(adv_id).append(bot_id)
            print(instr)
        elif instr_number == 3 or instr_number == 9:
            instr += str(instr_number) + " "
            adv_id = get_exist_adventure_id()
            if len(adventure_bottle_dict.get(adv_id)) == 0:
                count -= 1
                continue
            bot_id = get_adventure_s_bottle_id(adv_id)
            remove_adventure_s_bottle(adv_id, bot_id)
            instr += str(adv_id) + " " + str(bot_id)
            print(instr)
        elif instr_number == 4:
            instr += "4 "
            adv_id = get_exist_adventure_id()
            equ_id = get_new_id()
            instr += str(adv_id) + " "
            instr += str(equ_id) + " "
            instr += get_not_adv_name() + " "
            instr += str(random.randint(0, MAX_INIT_STAR))
            adventure_equipment_dict.get(adv_id).append(equ_id)
            print(instr)
        elif instr_number == 5 or instr_number == 10:
            instr += str(instr_number) + " "
            adv_id = get_exist_adventure_id()
            if len(adventure_equipment_dict.get(adv_id)) == 0:
                count -= 1
                continue
            equ_id = get_adventure_s_equipment_id(adv_id)
            remove_adventure_s_equipment(adv_id, equ_id)
            instr += str(adv_id) + " " + str(equ_id)
            print(instr)
        elif instr_number == 6:
            instr += "6 "
            adv_id = get_exist_adventure_id()
            if len(adventure_equipment_dict.get(adv_id)) == 0:
                count -= 1
                continue
            equ_id = get_adventure_s_equipment_id(adv_id)
            instr += str(adv_id) + " " + str(equ_id)
            print(instr)
        elif instr_number == 7:
            instr += "7 "
            adv_id = get_exist_adventure_id()
            foo_id = get_new_id()
            foo_name = get_not_adv_name()
            id_2_name_dict[foo_id] = foo_name
            instr += str(adv_id) + " "
            instr += str(foo_id) + " "
            instr += str(foo_name) + " "
            instr += str(random.randint(0, MAX_FOOD_ENERGY))
            adventure_food_dict.get(adv_id).append(foo_id)
            print(instr)
        elif instr_number == 8 or instr_number == 11:
            instr += str(instr_number) + " "
            adv_id = get_exist_adventure_id()
            if len(adventure_food_dict.get(adv_id)) == 0:
                count -= 1
                continue
            foo_id = get_adventure_s_food_id(adv_id)
            remove_adventure_s_food(adv_id, foo_id)
            instr += str(adv_id) + " " + str(foo_id)
            print(instr)
        elif instr_number == 12:
            instr += "12 "
            adv_id = get_exist_adventure_id()
            if len(adventure_bottle_dict.get(adv_id)) == 0:
                count -= 1
                continue
            bot_id = get_adventure_s_bottle_id(adv_id)
            # 判断该id是否是空的，空的则必然被携带
            if bot_id in empty_bottle_pool:
                remove_adventure_s_bottle(adv_id, bot_id)
            # 移除了该空瓶子
            bot_name = id_2_name_dict[bot_id]
            instr += str(adv_id) + " " + bot_name
            empty_bottle_pool.add(bot_id)
            print(instr)
        elif instr_number == 13:
            instr += "13 "
            adv_id = get_exist_adventure_id()
            if len(adventure_food_dict.get(adv_id)) == 0:
                count -= 1
                continue
            foo_id = get_adventure_s_food_id(adv_id)
            foo_name = id_2_name_dict[foo_id]
            instr += str(adv_id) + " " + foo_name
            print(instr)


# 以下的获取指随机取得
# 获取一个非adv的名字（可重复)
def get_not_adv_name():
    index = random.randint(0, len(not_adv_name_list) - 1)
    return not_adv_name_list[index]


# 获取一个adv名字，这个不可重复
def get_adv_new_name():
    name = str_random.getStr()
    while name in adventure_name_pool:
        name = str_random.getStr()
    adventure_name_pool.add(name)
    return name


# 获取一个已经被创建的冒险者id
def get_exist_adventure_id():
    index = random.randint(0, len(adventure_id_list) - 1)
    return adventure_id_list[index]


# 获取一个指定冒险者的瓶子id
def get_adventure_s_bottle_id(adv_id):
    bottle_id_list = adventure_bottle_dict.get(adv_id)
    index = random.randint(0, len(bottle_id_list) - 1)
    return bottle_id_list[index]


# ----
def get_adventure_s_equipment_id(adv_id):
    equipment_id_list = adventure_equipment_dict.get(adv_id)
    index = random.randint(0, len(equipment_id_list) - 1)
    return equipment_id_list[index]


# ----
def get_adventure_s_food_id(adv_id):
    food_id_list = adventure_food_dict.get(adv_id)
    index = random.randint(0, len(food_id_list) - 1)
    return food_id_list[index]


# 移除一个指定冒险者的指定 bot id
def remove_adventure_s_bottle(adv_id, bot_id):
    bottle_id_list = adventure_bottle_dict.get(adv_id)
    bottle_id_list.remove(bot_id)


# ----
def remove_adventure_s_equipment(adv_id, equ_id):
    equipment_id_list = adventure_equipment_dict.get(adv_id)
    equipment_id_list.remove(equ_id)


# ----
def remove_adventure_s_food(adv_id, foo_id):
    food_id_list = adventure_food_dict.get(adv_id)
    food_id_list.remove(foo_id)


# 随机生成一个新的id，并把他加入id池
# 保证id都是全新的
def get_new_id():
    new_id = random.randint(1, 1000000)
    while new_id in id_pool:
        new_id = random.randint(1, 1000000)
    id_pool.add(new_id)
    return new_id


init_env(3)
generate_instr(10)
