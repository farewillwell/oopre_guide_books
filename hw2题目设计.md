## 第四部分：题目描述

### 背景

本次作业基于pre1的内容开发，同学们应当在实现pre1题目所要求的内容的前提下基于pre1的代码完成本次作业。

好的，经过pre1的准备，我们的冒险者可以拥有一些装备和一些药水瓶，但是想要外出冒险，不可能带着所有的装备和所有的药水瓶瓶罐。因此在本次作业，我们新增了一个叫 **背包**的概念。

同时为了量化冒险者的状态，我们为冒险者引入了三个属性：体力（Hitpoint), 等级(level),战斗力(power)

体力代表冒险者当前的体力值，保证在测试中体力大于0。

等级代表冒险者的探索世界程度，决定了他的背包的容量（后面会详细规定）

战斗力为冒险者造成伤害的能力，在后面作业的迭代中会用到，在本次作业中，可以通过在背包里放装备实现增加战斗力。

同时，关于等级，我们需要引入食物类（food），通过食用食物可以提升冒险者的等级

在本次作业中，你需要实现的任务是：

- 在第一次作业的基础上完成冒险者新增加属性的管理
- 实现冒险者的背包功能，并依据要求约束实现背包内物品数目的控制

你可能要实现的类：

你可能需要实现的类和它们要拥有的属性

- Adventure ：ID，名字，等级(level)，体力(HitPoint)，战斗力(power)，药水瓶、食物和装备各自的容器
- Bottle：ID，名字，容量(capacity)，瓶子是否为空(isEmpty)
- Equipment：ID，名字，星级(star)
- Food：ID，名字，能量(energy)

**名字可以重复！！！**

### 背包限制

在pre1的作业里，我们定义了添加的概念（add），这个仅仅是让这名冒险者拥有了这个物品，但是他并没有携带这个物品。当且仅当这个物品在该名冒险者的背包中，才算他携带了这个物品。同时，当这个物品被删除时，冒险者视为不再拥有这个物品，因此也**不再携带**这个物品。

下面针对每类物品进行携带的规定。

特别的，我们规定，假设冒险者A尝试携带的物品B，本身已经被冒险者携带了，那么该条指令不会造成任何影响（不需要任何输出，同时冒险者A依旧携带物品B）

其中，药水瓶和食物都属于消耗品，需要设置方法：use

#### 装备

冒险者携带一件装备时，其战斗力+x（x为装备的星级）

当冒险者从背包中拿出这件装备时，其战斗力-x（x为装备的星级）

当冒险者未携带装备A但是拥有装备A时，装备A不对冒险者战斗力产生任何影响。

##### 限制

冒险者只能携带一件同名装备，例如他已经携带了name="sword"的装备（该装备id=1)，下一次再尝试携带另一个name="sword"的不同装备(该装备id=2)时，原本的name="sword"的装备（id=1)会被顶替，此时sword(id=1)依然属于该冒险者，但是认为冒险者携带了装备id=2而未携带装备id=1。同时，假如该冒险者没有携带名为"shoes"的装备，他可以携带name="shoes"的装备而不替换任何装备

#### 药水瓶

药水瓶需要增加属性：当前是否为空的布尔值：isEmpty

当冒险者携带药水瓶A时，他才能使用该药水瓶A。

use：冒险者使用该药水瓶，冒险者的体力增加x（如果该药水瓶的isEmpty==true，那么x为药水瓶的容积，否则x为0），该药水瓶变空(isEmpty==true)。同时冒险者仍然拥有并携带该药水瓶。

当药水瓶为空(isEmpty==true)的时候，假设有操作使用该药水瓶，则冒险者的体力增加为0，按照成功使用判定，同时，为了给继续携带药水瓶腾出空间，在进行上述的成功使用操作后，冒险者将丢弃该空药水瓶，该药水瓶将不再被该冒险者拥有并且不再被该冒险者携带，对于这个丢弃行为不需要进行输出。

##### 限制

冒险者只能携带x个同名药水瓶（x为当前1+（冒险者等级/5））。

例如，冒险者等级为19,19/5=3，x=1+3=4，因此冒险者只能携带4个同名药水瓶.

假设在这时，冒险者携带了同名药水瓶water*4（id为1,2,3,4），之后再尝试携带water（id为5），不会产生任何状态上的影响，也不用产生任何输出，他依旧携带1234，id=5依旧属于他而不被他携带。

#### 食物

当冒险者携带食物A的时候，他才能食用该食物A。

背包对食物的数量没有任何限制

use：消耗掉该食物（从此该食物**不再属于**该冒险者，同时也不再被携带），之后冒险者的等级提升x(x为食物的能量)

比如冒险者当前1级，拥有食物A，A的能量=2，携带后使用食物A，则冒险者不再拥有食物A，同时冒险者的等级变为3

## 操作要求

在本次作业中，初始时，你没有需要管理的冒险者，我们通过若干条操作指令来修改当前的状态：

（**第2-6条同第一次作业**）

1. 加入一个需要管理的冒险者（新加入的冒险者不携带任何药水瓶、食物和装备，并且等级为1，初始血量为100，战斗力为0）
2. 给某个冒险者增加一个药水瓶
3. 删除某个冒险者的某个药水瓶
4. 给某个冒险者增加一个装备
5. 删除某个冒险者的某个装备
6. 给某个冒险者的某个装备提升一个星级
7. 给冒险者增加一个食物
8. 删除冒险者的一个食物
9. 冒险者尝试携带他拥有的某件装备
10. 冒险者尝试携带他拥有的某个药水瓶
11. 冒险者尝试携带他拥有的某个食物
12. 冒险者使用某个药水瓶
14. 冒险者使用某个食物

其中，提升星级的意思是，新星级=原有星级+1

**值得注意的是，在12和13中，我们采用name来确定所使用的物品。假设当前冒险者携带了多个同名物品，则使用id最小的那个。**

例如，冒险者携带食物name=fish，id=2，再携带食物name=fish，id=1，冒险者使用食物name=fish的时候，会使用id=1而不使用id=2

同时，使用某name的物品时，假设冒险者没有携带名字为该name的物品，则视为使用失败，不产生任何影响。

当冒险者不再拥有某件物品时，若原本这件物品已经被该冒险者携带，则自动认为他不再携带该物品

关于12/13的输出，我们规定分为两种情况考虑：

##### 使用药水输出：

成功：一个整数A+一个空格+一个整数B，整数A为使用药水瓶的id，整数B为该冒险者使用该药水瓶后的体力值

例：冒险者id=1，当前他的体力为100他拥有并携带了id=2,name="water",capacity=20  ,isEmpty = false（假设他仅仅拥有这一个同名药水瓶）的药水瓶，则当输入为：

```
12 1 water
```

应当输出：

```
2 120
```

现在的name="water"的药水瓶的isEmpty属性变成了true,假设要再次使用该药水瓶，也就是接下来输入：

```
12 1 water
```

冒险者会正常使用该药水瓶，并在之后将该药水瓶丢弃，应当输出：

```
2 120
```

同时，该药水瓶被丢弃，不再属于该冒险者，冒险者也不再携带该药水瓶

但是，另一个情景：

失败：输出 "fail  to use"+一个空格+name(其中name为输入中的name)

假设 冒险者id=1，当前他的体力为100他**没有携带**id=2,name="water"的药水瓶，则对应输出为：

```
fail to use water
```

##### 使用食物输出

成功：一个整数A+一个空格+一个整数B，整数A为使用食物的id，整数B为该冒险者使用该食物后的等级

例：冒险者id=1，当前他的等级为10，他拥有并携带了id=2,name="fish",energy=2（假设他仅仅拥有这一个同名食物）的食物，则当输入为：

```
13 1 fish
```

应当输出：

```
2 12 
```

但是，另一个情景：

失败：输出 "fail  to eat"+一个空格+name(其中name为输入中的name)(注意，这里是**eat**)

假设 冒险者id=1，当前他的体力为100他拥有但**没有携带**id=2,name="water",capacity=20（假设他仅仅拥有这一个同名食物）的食物，则对应输出为：

```
fail to eat fish
```

##### 

### 输入格式

第一行一个整数 *n*，表示操作的个数。

接下来的 n 行，每行一个形如 `{type} {attribute}` 的操作，`{type}` 和 `{attribute}` 间、若干个 `{attribute}` 间使用**若干**个空格分割，操作输入形式及其含义如下

| type | attribute                             | 意义                                                         | 输出（每条对应占一行）                                       |
| ---- | ------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1    | `{adv_id} {name}`                     | 加入一个 ID 为 `{adv_id}`、名字为 `{name}` 的冒险者          | 无                                                           |
| 2    | `{adv_id} {bot_id} {name} {capacity}` | 给 ID 为 `{adv_id}` 的冒险者增加一个药水瓶，药水瓶的 ID、名字、容量分别为 `{bot_id}`、`{name}`、`{capacity}`，**且默认为已装满**(`isEmpty`==`false`) | 无                                                           |
| 3    | `{adv_id} {bot_id}`                   | 将 ID 为 `{adv_id}` 的冒险者的 id 为 `{bot_id}` 的药水瓶删除 | 一个整数+一个空格+一个字符串，整数为删除后冒险者药水瓶数目，字符串为删除的药水瓶的name |
| 4    | `{adv_id} {equ_id} {name} {star}`     | 给 ID 为 `{adv_id}` 的冒险者增加一个装备，装备的 ID、名字、星级分别为 `{equ_id}`、`{name}`、`{star}` | 无                                                           |
| 5    | `{adv_id} {equ_id}`                   | 将 ID 为 `{adv_id}` 的冒险者的 id 为 `{equ_id}` 的装备删除   | 一个整数+一个空格+一个字符串，整数为删除后冒险者装备数目，字符串为删除的装备的name |
| 6    | `{adv_id} {equ_id}`                   | 将 ID 为 `{adv_id}` 的冒险者的 id 为 `{equ_id}` 的装备提升一个星级 | 一个字符串+一个空格+一个整数，字符串为装备的name，整数为装备升星后的星级 |
| 7    | `{adv_id} {food_id} {name} {energy}`  | 给 ID 为 `{adv_id}` 的冒险者增加一个食物，食物的 ID、名字、能量分别为 `{equ_id}`、`{name}`、`{energy}` | 无                                                           |
| 8    | `{adv_id} {food_id}`                  | 将 ID 为 `{adv_id}` 的冒险者的 id 为 `{food_id}` 的食物删除  | 一个整数+一个空格+一个字符串，整数为删除后冒险者食物数目，字符串为删除的食物的name |
| 9    | `{adv_id} {equ_id}`                   | ID 为 `{adv_id}` 的冒险者尝试携带ID为 `{equ_id}` 的装备      | 无                                                           |
| 10   | `{adv_id} {bot_id}`                   | ID 为 `{adv_id}` 的冒险者尝试携带ID为 `{bot_id}` 的瓶子      | 无                                                           |
| 11   | `{adv_id} {food_id}`                  | ID 为 `{adv_id}` 的冒险者尝试携带ID为 `{food_id}` 的食物     | 无                                                           |
| 12   | `{adv_id} {name}`                     | ID 为 `{adv_id}` 的冒险者尝试使用他拥有的名字为`{name}`的药水瓶 | 成功：一个整数A+一个空格+一个整数B，整数A为该被使用药水瓶的id，整数B为该冒险者使用该药水瓶后的体力值                                                                                 失败： "fail  to use"+一个空格+name(其中name为输入中的name) |
| 13   | `{adv_id} {name}`                     | ID 为 `{adv_id}` 的冒险者尝试使用他拥有的名字为`{name}`的食物 | 成功：一个整数A+一个空格+一个整数B，整数A为该食物的id，整数B为该冒险者使用该食物后的等级                  失败：  "fail  to eat"+一个空格+name(其中name为输入中的name) |

### 数据限制

##### 变量约束

| 变量       | 类型   | 说明                     |
| ---------- | ------ | ------------------------ |
| `id `      | 整数   | 取值范围：0 - 2147483647 |
| `name`     | 字符串 | 保证不会出现空白字符     |
| `capacity` | 整数   | 取值范围：0 - 2147483647 |
| `star`     | 整数   | 取值范围：0 - 2147483647 |
| `level`    | 整数   | 取值范围：1 - 2147483647 |
| `HitPoint` | 整数   | 取值范围：0 - 2147483647 |
| `power`    | 整数   | 取值范围：0 - 2147483647 |
| `isEmpty`  | 布尔值 | 取值范围:   true/false   |
| `energy`   | 整数   | 取值范围： 0-2147483647  |

##### 操作约束

1. **保证所有的冒险者、药水瓶、装备、食物id均不相同**
2. 保证删除了的药水瓶/装备/食物的id不会再次出现
3. 2-6保证所有冒险者均已存在
4. 3/5/6保证该冒险者拥有操作中提到id的药水瓶/装备/食物
5. 保证增加的装备，食物和药水瓶原本不存在
6. 操作数满足1≤*n*≤2000
7. 9-11保证该冒险者拥有操作中提到id的药水瓶/装备/食物
8. 12-13 **不**保证以提到的name为名字的物品已经被携带









