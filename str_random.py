import random

base_str = 'ABCDEFGHIGKLMNOPQRSTUVWXYZabcdefghigklmnopqrstuvwxyz0123456789^&*%$#!'

str_min_len = 5
str_max_len = 10
random_range = len(base_str)


def getStr():
    length = random.randint(str_min_len, str_max_len)
    data_str = ""
    for i in range(0, length):
        now_index = random.randint(0, random_range-1)
        data_str += base_str[now_index]
    return data_str
