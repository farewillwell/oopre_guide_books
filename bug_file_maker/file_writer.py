import os

import Class1Strings
import Class2Strings
import Class3Strings
import Class4Strings
import mainString

file_number_of_class1 = 0
file_number_of_class2 = 0
file_number_of_class3 = 0
file_number_of_class4 = 0

# 学生人数
student_number = 200


# 4*4*4*4 = 256，够了
def set_file_number(order):
    file_number_of_class1 = order % 4
    order //= 4
    file_number_of_class2 = order % 4
    order //= 4
    file_number_of_class3 = order % 4
    order //= 4
    file_number_of_class4 = order % 4


Class1Strings.init_class1()
Class2Strings.init_class2()
Class3Strings.init_class3()
Class4Strings.init_class4()
mainString.init_main()
if not os.path.exists("./homeworks"):
    os.makedirs("./homeworks")
for i in range(0, student_number):
    if not os.path.exists("./homeworks/oopre_hw4_%d" % i):
        os.makedirs("./homeworks/oopre_hw4_%d" % i)
    set_file_number(i)
    with open("./homeworks/oopre_hw4_%d/Class1.java" % i, 'w') as file1:
        print(Class1Strings.codes[file_number_of_class1], file=file1)
        file1.close()
    with open("./homeworks/oopre_hw4_%d/Class2.java" % i, 'w') as file2:
        print(Class2Strings.codes[file_number_of_class2], file=file2)
        file2.close()
    with open("./homeworks/oopre_hw4_%d/Class3.java" % i, 'w') as file3:
        print(Class3Strings.codes[file_number_of_class3], file=file3)
        file3.close()
    with open("./homeworks/oopre_hw4_%d/Class4.java" % i, 'w') as file4:
        print(Class4Strings.codes[file_number_of_class4], file=file4)
        file4.close()
    with open("./homeworks/oopre_hw4_%d/Main.java", 'w') as fileMain:
        print(mainString.main_string, file=fileMain)
        fileMain.close()
